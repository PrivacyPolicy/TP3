DELIMITER //

CREATE PROCEDURE agregar_jugador_unico(IN p_id_jugador int, IN p_cant_cartas varchar(50))

BEGIN

DELETE FROM Jugadores;

INSERT INTO Jugadores(id_jugador, cantidad_cartas) values (p_id_jugador, p_cant_cartas);

END//

DELIMITER ;