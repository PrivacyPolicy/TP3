
package tp3;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Conexion {
    
    private final int idJugador;
    private final int cant;

    Conexion(int idJugador, int cant) {
        this.idJugador = idJugador;
        this.cant = cant;
    }
    
    public void conectar() throws InstantiationException, IllegalAccessException, SQLException{
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        
            java.sql.Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Tp3?user=root&password=Heloraktia1990");
            

            PreparedStatement ps = conn.prepareStatement("call agregar_jugador_unico (?,?)");
            ps.setInt(1, idJugador);
            ps.setInt(2, cant);
            boolean result = ps.execute();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
