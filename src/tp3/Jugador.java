/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;


import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Jugador extends Observable implements Runnable{
    private Repartidor dealer;
    private int idJugador;
    private List<Carta> propias = new ArrayList<>();


    
    public Jugador(Repartidor dealer, int idDealer) 
    {
        this.dealer = dealer;
        this.idJugador = idDealer;
    }
    
    @Override
    public void run() {
        while(!this.dealer.getMazo().getListaDeCartas().isEmpty()){ //Hay que cambiar esto para delegar esta responsabilidad a Repartidors

            Carta carta = null;
             try {
                carta = this.dealer.get();
            } catch (Exception ex) {
                Logger.getLogger(Jugador.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(carta!=null){
                
            this.propias.add(carta);
              
            }
            setChanged();
            notifyObservers(carta);
            
            try
                {
                    Thread.sleep(0);
                } 
            catch (InterruptedException e) 
                {
                    System.err.println("Jugador: " + idJugador + ": Error en run -> " + e.getMessage());
                }
            }
        if(this.dealer.getMazo().getListaDeCartas().isEmpty()){
            System.out.println("Jugador: " + this.idJugador + " tiene " + this.propias.size() + " cartas");
            dealer.valoresFinales(this);

           
        }
            
    }

    public Repartidor getDealer() {
        return dealer;
    }

    public int getIdDealer() {
        return idJugador;
    }
    
    public int getCantCartas(){
    return this.propias.size();
}
    
}

    

    
    

