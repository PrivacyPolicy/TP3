
package tp3;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class Juez implements Observer{
    static private Map<Integer, Integer> totalCarta = new HashMap<>();

    
    @Override
    public synchronized void update(Observable repartidor, Object jugador) {
        if(jugador instanceof Jugador){
            Jugador nuevo = (Jugador) jugador;
            totalCarta.put(nuevo.getIdDealer(), nuevo.getCantCartas());

        }        
    }
    
    public int ganador() throws InstantiationException, IllegalAccessException, SQLException{
        int aux = 0;
        int idGanador = 0;

        for(int x=1;x<totalCarta.size()+1;x++){
            if(totalCarta.get(x)>aux){
                aux=totalCarta.get(x);
                idGanador=x;
            }
        }
        System.out.println("El Ganador es el Jugador: " + idGanador);
        Conexion connect = new Conexion(idGanador, totalCarta.get(idGanador));
        connect.conectar();
        return idGanador;

    }
    
    
}
