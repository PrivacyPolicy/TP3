/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
 
/**
 *
 * @author martinabancens
 */
public class Mazo {
    private List<Carta> listaDeCartas = new ArrayList<>();
    
    
    public Mazo() {
        this.listaDeCartas.add(new Carta("A", "Rombo"));
        this.listaDeCartas.add(new Carta("2", "Rombo"));
        this.listaDeCartas.add(new Carta("3", "Rombo"));
        this.listaDeCartas.add(new Carta("4", "Rombo"));
        this.listaDeCartas.add(new Carta("5", "Rombo"));
        this.listaDeCartas.add(new Carta("6", "Rombo"));
        this.listaDeCartas.add(new Carta("7", "Rombo"));
        this.listaDeCartas.add(new Carta("8", "Rombo"));
        this.listaDeCartas.add(new Carta("9", "Rombo"));
        this.listaDeCartas.add(new Carta("10", "Rombo"));
        this.listaDeCartas.add(new Carta("J", "Rombo"));
        this.listaDeCartas.add(new Carta("Q", "Rombo"));
        this.listaDeCartas.add(new Carta("K", "Rombo"));
        this.listaDeCartas.add(new Carta("A", "Pica"));
        this.listaDeCartas.add(new Carta("2", "Pica"));
        this.listaDeCartas.add(new Carta("3", "Pica"));
        this.listaDeCartas.add(new Carta("4", "Pica"));
        this.listaDeCartas.add(new Carta("5", "Pica"));
        this.listaDeCartas.add(new Carta("6", "Pica"));
        this.listaDeCartas.add(new Carta("7", "Pica"));
        this.listaDeCartas.add(new Carta("8", "Pica"));
        this.listaDeCartas.add(new Carta("9", "Pica"));
        this.listaDeCartas.add(new Carta("10", "Pica"));
        this.listaDeCartas.add(new Carta("J", "Pica"));
        this.listaDeCartas.add(new Carta("Q", "Pica"));
        this.listaDeCartas.add(new Carta("K", "Pica"));
        this.listaDeCartas.add(new Carta("A", "Corazones"));
        this.listaDeCartas.add(new Carta("2", "Corazones"));
        this.listaDeCartas.add(new Carta("3", "Corazones"));
        this.listaDeCartas.add(new Carta("4", "Corazones"));
        this.listaDeCartas.add(new Carta("5", "Corazones"));
        this.listaDeCartas.add(new Carta("6", "Corazones"));
        this.listaDeCartas.add(new Carta("7", "Corazones"));
        this.listaDeCartas.add(new Carta("8", "Corazones"));
        this.listaDeCartas.add(new Carta("9", "Corazones"));
        this.listaDeCartas.add(new Carta("10", "Corazones"));
        this.listaDeCartas.add(new Carta("J", "Corazones"));
        this.listaDeCartas.add(new Carta("Q", "Corazones"));
        this.listaDeCartas.add(new Carta("K", "Corazones"));
        this.listaDeCartas.add(new Carta("A", "Trebol"));
        this.listaDeCartas.add(new Carta("2", "Trebol"));
        this.listaDeCartas.add(new Carta("3", "Trebol"));
        this.listaDeCartas.add(new Carta("4", "Trebol"));
        this.listaDeCartas.add(new Carta("5", "Trebol"));
        this.listaDeCartas.add(new Carta("6", "Trebol"));
        this.listaDeCartas.add(new Carta("7", "Trebol"));
        this.listaDeCartas.add(new Carta("8", "Trebol"));
        this.listaDeCartas.add(new Carta("9", "Trebol"));
        this.listaDeCartas.add(new Carta("10", "Trebol"));
        this.listaDeCartas.add(new Carta("J", "Trebol"));
        this.listaDeCartas.add(new Carta("Q", "Trebol"));
        this.listaDeCartas.add(new Carta("K", "Trebol"));
        
        
        
        
    }
    
    public Carta removeLast(){
        Carta aux = this.listaDeCartas.get(this.listaDeCartas.size()-1);
        this.listaDeCartas.remove(this.listaDeCartas.size() - 1); 
        return aux;
    }
    
    public Carta getLast() throws Exception{
        if(!this.listaDeCartas.isEmpty())
        return this.listaDeCartas.get(this.listaDeCartas.size()-1);
        
        else{
            throw new Exception("No Hay mas cartas");
        }
    }

    public List<Carta> getListaDeCartas() {
        return this.listaDeCartas;
    }

   

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mazo other = (Mazo) obj;
        if (!Objects.equals(this.listaDeCartas, other.listaDeCartas)) {
            return false;
        }
        return true;
    }
    
    

    @Override
    public String toString() {
        return "Mazo{" + "Valor=" + listaDeCartas + '}';
    }
   
    
}
