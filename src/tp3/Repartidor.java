/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martinabancens
 */
public class Repartidor extends Observable implements Observer{
    private Mazo mazo;


    public Repartidor(Mazo mazo) {
        this.mazo = mazo;
    }
    
    
    
    public synchronized Carta get() throws Exception
    {
        Carta aux = null;
        if(!this.mazo.getListaDeCartas().isEmpty()){
            if (this.mazo.getListaDeCartas().size()>0){   
                aux = this.mazo.removeLast();
                notify();
            }
        else{
            throw new Exception("Mazo Vacio");
        }
    }
    return aux;
        
        
    }
    
    @Override
    public synchronized void update(Observable jugadorNuevo, Object carta) {
        Jugador nuevo = (Jugador) jugadorNuevo;
        if(carta  instanceof Carta){
            if(carta!=null){
                
                Carta cartita = (Carta) carta;
                try {
                    System.out.println("Jugador: "+ nuevo.getIdDealer() + " Carta: " + cartita.getValor()+ " " + cartita.getPalo());
                } catch (Exception ex) {
                    Logger.getLogger(Repartidor.class.getName()).log(Level.SEVERE, null, ex);
                }
            
            }
        }
        else{
            System.out.println("Jugador: " + nuevo.getIdDealer() + " finalizado");
        }
    }
    
    public synchronized void valoresFinales(Jugador jugador){
        setChanged();
        notifyObservers(jugador);
    }
    
    public Mazo getMazo(){
        return this.mazo;
    }
    

    
}
