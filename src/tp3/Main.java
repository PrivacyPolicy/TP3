/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.sql.SQLException;
import java.util.Observer;

/**
 *
 * @author martinabancens
 */
public class Main {
    private static Repartidor dealer;
    private static Juez juez;

    private static Thread player1;
    private static Thread player2;
    private static Thread player3;
    private static Thread player4;

    private static Jugador p1;
    private static Jugador p2;
    private static Jugador p3;
    private static Jugador p4;

   
    public static void main(String[] args) throws InterruptedException, InstantiationException, IllegalAccessException, SQLException {
        Mazo mazo = new Mazo();
        dealer = new Repartidor(mazo);
        juez = new Juez();

        p1 = new Jugador((Repartidor) dealer, 1);
        p2 = new Jugador((Repartidor) dealer, 2);
        p3 = new Jugador((Repartidor) dealer, 3);
        p4 = new Jugador((Repartidor) dealer, 4);
        
        p1.addObserver(dealer);
        p2.addObserver(dealer);
        p3.addObserver(dealer);
        p4.addObserver(dealer);
        
        dealer.addObserver(juez);
        
        
        player1 = new Thread(p1);
        player2 = new Thread(p2);
        player3 = new Thread(p3);
        player4 = new Thread(p4);
                     
        player1.start();
        player2.start();
        player3.start();
        player4.start();
        
        player1.join();
        player2.join();
        player3.join();
        player4.join();

        
        juez.ganador();
        
       
    }
}
